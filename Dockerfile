FROM alpine:3.9 AS base

FROM base AS build-base
RUN apk add --no-cache curl git

FROM build-base AS kubectl
ARG KUBECTL_VERSION=1.11.9
ARG KUBECTL_CHECKSUM=946b186715234df92fbea3b77e2fbbe7bb2a8498af8103316514e1612ab6895f
ARG SOURCE=https://storage.googleapis.com/kubernetes-release/release/v$KUBECTL_VERSION/bin/linux/amd64/kubectl
ARG TARGET=/usr/bin/kubectl
RUN curl -fLSs "$SOURCE" -o "$TARGET"
RUN sha256sum "$TARGET"
RUN echo "$KUBECTL_CHECKSUM *$TARGET" | sha256sum -c -
RUN chmod +x "$TARGET"

FROM build-base AS helm
ARG HELM_VERSION=2.13.1
ARG HELM_CHECKSUM=c1967c1dfcd6c921694b80ededdb9bd1beb27cb076864e58957b1568bc98925a
ARG SOURCE=https://storage.googleapis.com/kubernetes-helm/helm-v$HELM_VERSION-linux-amd64.tar.gz
ARG TARGET=/helm.tar.gz
RUN curl -fLSs "$SOURCE" -o "$TARGET"
RUN sha256sum "$TARGET"
RUN echo "$HELM_CHECKSUM *$TARGET" | sha256sum -c -
RUN mkdir -p /helm
RUN tar -xvf "$TARGET" -C /helm

FROM build-base AS stage
WORKDIR /stage
ENV PATH=$PATH:/stage/usr/bin
COPY --from=kubectl /usr/bin/kubectl ./usr/bin/
COPY --from=helm /helm/linux-amd64/helm ./usr/bin/
COPY --from=helm /helm/linux-amd64/tiller ./usr/bin/

RUN set -x \
  && helm version --client \
  && kubectl version --client \
  && exit 0

FROM base
RUN apk add --no-cache ca-certificates git
COPY --from=stage /stage/ /
